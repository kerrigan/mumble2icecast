# This is [Mumble](www.mumble.info) to [Icecast](www.icecast.org) streaming software

## Preparations

1. You must have pulseaudio (yes it works using it)
2. Install dependenices
   1. Runtime dependencies:
> `sudo apt-get install -y --no-install-recommends liquidsoap libopenal1 pulseaudio pulseaudio-utils libopus0 liquidsoap-plugin-vorbis liquidsoap-plugin-taglib liquidsoap-plugin-pulseaudio liquidsoap-plugin-ogg liquidsoap-plugin-icecast liquidsoap-plugin-samplerate`
   2. Build dependencies:
   > `sudo apt-get install -y --no-install-recommends libopenal-dev libopus-dev`
3. Build `mheadless` ([gumble](https://github.com/layeh/gumble) based simple mumble client)
> make
4. Edit configurations
  > `mheadless.json` - mumble server settings
  > `authdata.liq` - icecast settings
  
5. Change `background.ogg` to background which you like

## Start
Run `run.sh` script
