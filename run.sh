#!/bin/bash -e
SINK_ID=`pactl load-module module-null-sink sink_name=mumble_stream`
PULSE_SINK=mumble_stream ./mumble-headless -c mheadless.json &
MHEADLESS_PID=$!

liquidsoap mpdmixer.liq > /dev/null 2>&1

#kill -1 $MHEADLESS_PID

pacmd unload-module $SINK_ID
